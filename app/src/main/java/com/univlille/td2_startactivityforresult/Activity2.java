package com.univlille.td2_startactivityforresult;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class Activity2 extends AppCompatActivity {

    /**
     * cette activité sera appelée par MainActivity, puis va récupérer le texte envoyé par
     * MainActivity, et enfin renverra à MainActivity le texte présent dans la zone de saisie
     */

    private EditText editText ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        editText = findViewById(R.id.editTextActivity2);

        // on récupère l'intent qui correspond à cette activité
        Intent intent = getIntent();

        // puis on va chercher le paramètre envoyé par MainActivity (ici, on suppose qu'il
        // existe, mais on devrait le vérifier sinon.. crash possible)
        editText.setText(intent.getStringExtra("envoiVersActivity2"));

        Button btnActivity2 = findViewById(R.id.btnActivity2);
        btnActivity2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                String message = editText.getText().toString();

                // au clic sur le bouton, on crée un intent pour le retour vers MainActivity
                Intent intentRetour = new Intent();

                // comme dans MainActivity, on crée une variable qu'on stocke dans l'intent et
                // qui est automatiquement associée à 'data' dans le retour géré dans MainActivity
                // dans onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
                intentRetour.putExtra("MESSAGE", message);

                // on indique dans l'intent que tout s'est bien passé
                setResult(RESULT_OK, intentRetour);

                // on termine l'activité
                finish();
            }
        });
    }
}

