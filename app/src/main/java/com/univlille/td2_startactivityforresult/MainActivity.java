package com.univlille.td2_startactivityforresult;


import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Cet exemple montre comment appeler une activité en lui passant un paramètre (ici le contenu d'une zone texte)
 * et récupérer le texte renvoyé à la fermeture de la seconde activité, tout en gérant les différents
 * retours (texte saisi, texte vide, retour par 'back')
 */

public class MainActivity extends AppCompatActivity {

    // Code qu'on associe à l'activité qu'on appelle. Ce n'est qu'un chiffre dont la valeur n'a pas
    // d'importance. On aurait pu mettre 1, 2, 57, ou tout autre valeur. Il faut juste que cette
    // valeur soit unique.
    private static final int CODE_ACTIVITE2 = 100;

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.txtMainActivity);

        findViewById(R.id.btnMainActivity).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                // on crée un Intent sur l'activité 2. On peut voir ça comme
                // l'intention(Intent) d'appeler l'activité2

                // pour le contexte en 1er paramètre, on aurait pu aussi mettre :
                // Intent activity2 = new Intent(v.getContext(), Activity2.class);

                Intent activity2 = new Intent(MainActivity.this, Activity2.class);

                // on crée une variable sous la forme (clé,valeur) qu'on stocke dans l'intent
                // pour la passer en paramètre à l'activité2
                activity2.putExtra("envoiVersActivity2", textView.getText());

                // on démarre l'activité 2 et on attend un retour de sa part.
                // Si on voulait juste la lancer sans s'occuper du retour, on aurait juste fait
                // startActivity(activity2);
                startActivityForResult(activity2, CODE_ACTIVITE2);
            }
        });
    }

    // Ici, on traite le retour après la terminaison de l'activité 2
    // L'appel de la méthode ci-dessous est fait automatiquement par Android quand une activité
    // appelée depuis ici (c-a-d MainActivity dans le cas présent) se termine.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // on regarde tout d'abord quelle activité s'est terminée
        switch (requestCode) {

            case CODE_ACTIVITE2:
                // traitement du retour de l'activité 2

                // on regarde maintenant comment s'est terminée l'activité 2
                switch (resultCode) {

                    // si elle s'est bien terminée (c-à-d avec un finish...)
                    case RESULT_OK:
                        // on regarde si elle a renvoyée des données
                        if (data != null) {
                            // Dans l'absolu, on devrait aussi tester l'existence de la clé "MESSAGE" ! (ou try-catch...)

                            // on a donc des data qui ont été renvoyées...
                            // maintenant on va chercher celle dont on a besoin
                            String message = data.getStringExtra("MESSAGE");

                            // on vérifie sa valeur...
                            if (message.equals("")) {
                                // si on a renvoyé un texte vide, on affiche un texte qui le dit
                                textView.setText("pas de message");
                            }
                            // si on renvoyé un texte non vide, on l'affiche
                            else textView.setText(message);
                        }
                        return;

                    // si l'activité 2 s'est mal terminée (c-à-d avec la touche back...)
                    case RESULT_CANCELED:
                        // on affiche un texte qui le dit
                        textView.setText("annulation");
                        return;

                    default:
                        return;
                }

            default:
                return;
        }
    }
}